const fs = require('fs');
describe('test google.com', () => {
    const {
        Builder,
        By,
        Key,
        until
    } = require('selenium-webdriver');
    var driver;
 
    beforeEach(() => {
        driver = new Builder()
            .forBrowser('firefox')
            .build();
    });
 
    afterEach(() => {
        driver.quit();
    });
 
    it('should open our error.html page', async () => {
        // get to our index html page
        await driver.get('https://dev-s3-bucket-01.s3.us-east-2.amazonaws.com/index.html');
        // 
        await driver.findElement(By.id("error-link")).click().then(() => {
            driver.wait(() => {
                driver.getTitle.then(title => expect(title).toEqual("Errr, Congrats!"))
            })
        })
    });
            // .getTitle()
            // .then(title => {
            //     expect(title).toEqual('Error, Congrats!');
            // });


        /*
        //1 load up the url
    driver.get( testConfig.url + '/britain/england/cornwall/hotel88').then(function(){
        //2 get current page html title
        var title = driver.findElement(By.css('title'));
        title.getInnerHtml().then(function(html) {
            var controlTestTitle = html;
            //3 now move onto the owners link
            driver.findElement(By.css('.details-web')).click().then(function(){
                driver.wait(function(){
                    return driver.isElementPresent(By.css("title"));
                }, testConfig.timeout).then(function(){
                    var title = driver.findElement(By.css('title'));
                    title.getInnerHtml().then(function(html) {
                        //check that this title is not the same
                        if( html != controlTestTitle ){
                            if (callback) {
                                callback(callback);
                            }
                        } else {
                            throw 'the page title did not change when moving onto the owners website';
                        }
                    });
                });
            });
        });
    });

        */
 
    // it('should open google search and view search results', async () => {
    //     await driver.get('http://www.google.com');
    //     var element = await driver.findElement(By.css('input[title=Search]'));
    //     await element.sendKeys("selenium", Key.RETURN);
    //     await driver.wait(until.titleContains("selenium"), 4000);
    //     driver
    //         .getTitle()
    //         .then(title => {
    //             expect(title).toEqual('selenium - Google Search');
    //         });
    // });
 
    // it('should open google search and do image search', async () => {
    //     await driver.get('http://www.google.com');
    //     var element = await driver.findElement(By.css('input[title=Search]'));
    //     await element.sendKeys("selenium", Key.RETURN);
    //     await driver.wait(until.titleContains("selenium"), 4000);
    //     var imageSearch = driver.findElement(By.xpath("//a[contains(text(), 'Images')]"));
    //     await imageSearch.click();
    //     let image = await driver.takeScreenshot();
    //     fs.writeFileSync('out.png', image, 'base64');
 
    // });
 
});