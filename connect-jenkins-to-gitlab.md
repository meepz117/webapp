# Guide to connecting Jenkins to a GitHub Repo

## Resources/Tutorials used

[Video Tutorial](https://www.youtube.com/watch?v=SWl_XicACyk&t=997s)

- Skip to 6:30. Prior to that is him setting up Jenkins for the first time, irrelvant if you already have Jenkins setup.
- This video is great. The only issue I had is that the plugin he uses is outdated ("GitLab Hook Plugin"). No problem, the plugin we want is called "GitLab Plugin".
- Rather than the plugin this tutorial gave me no issues. It also goes into setting up your ssh keys, which when I followed the next link I messed up.

[Simplified Article](https://docs.bitnami.com/tutorials/create-ci-pipeline/#step-1-create-a-gitlab-project)

- This Tutorial is simplied, for example it does not go into detail on how to create ssh keys. So if you really know what you are doing go on and follow this. Personally, I followed it and it did not work because I messed up setting up the ssh keys.
- It does provide this benefit. Midway step 5 and step 6 goes into how you can set it up so if it builds then it will throw the code into the production branch.